# Author : Ajinkya Kadam
# Email : ajinkya.kadam@nyu.edu
# Script to execute the experiments
# Please change the source,sink and router port numbers as per your configuration
# Remember to add your username and ipaddress for nodes at line 15 and 16

#!/bin/bash

limit=10
sourcePort=						##like		"30268"
sinkPort=						##like 		"30267"
routerPort=						##like 		"30266"
max_limit=80
experimentDuration=62000
packetSize=512
lambda=220
username=						##like	 	"zzz000"
ipnodes=						##like 		"pc5.geni.kettering.edu"	

now=$(date +'%Y-%m-%d-%H-%M')


##Copy the repective files to router1 node
echo "Copying files to destination nodes"

scp -P $routerPort -o "StrictHostKeyChecking no" qmon.sh $username@$ipnodes:/users/$username

scp -P $routerPort -o "StrictHostKeyChecking no" qexec.sh  $username@$ipnodes:/users/$username

echo "Finished copying files!!"

sleep 1s

echo -en '\n'

##Execute experiments
echo "Experiment exection started"

for i in {1..1}
do

	ssh -o "StrictHostKeyChecking no" $username@$ipnodes -p "$sinkPort" "mkdir receiverlog-$i-$now; ITGRecv" &		
	sleep 2s
	while [[ "$limit" -le "$max_limit" ]];
	do  
		for run in {1..10}
		do 
		  sleep 2s
		  echo "Executing experiment for queue limit as $limit with the run number as $run"
		  senderLog=sender_"$limit"_"$run".txt	
		  receiverLog=receiver_"$limit"_"$run".txt
		  dumpOutput=router_"$limit"_"$run".txt
		  echo "Sender log is saved in $senderLog"
		  echo "Receiver log is saved in $receiverLog"
		  echo "Queue log is saved in $dumpOutput"
		  echo -en '\n'
		  ssh -o "StrictHostKeyChecking no" $username@$ipnodes -p "$sourcePort" "mkdir senderlog-$i-$now; ITGSend -a sink1 -l senderlog-$i-$now/$senderLog -x receiverlog-$i-$now/$receiverLog -E $lambda -e $packetSize -t $experimentDuration -T UDP" &
		  ssh -o "StrictHostKeyChecking no" $username@$ipnodes -p "$routerPort" "mkdir output-$i-$now; bash qexec.sh "$limit"; bash qmon.sh eth2 60 0.1 | tee output-$i-$now/$dumpOutput" &
		  sleep 80s		
		done
		run=1
		limit=$(($limit+5))
		echo "Updating queue size to $limit"
			
	done
	echo "Replication $i done. Now initialize the parameters again and do next replication"
	echo -en '\n'
	echo "Copying the output for this replication to your node."	
	echo -en '\n'
	ssh -o "StrictHostKeyChecking no" $username@$ipnodes -p "$sourcePort" "tar -czvf senderlog-$i-$now.tgz senderlog-$i-$now"
	scp -P $sourcePort -o "StrictHostKeyChecking no" $username@$ipnodes:/users/$username/senderlog-$i-$now.tgz . 
	ssh -o "StrictHostKeyChecking no" $username@$ipnodes -p "$sinkPort" "tar -czvf receiverlog-$i-$now.tgz receiverlog-$i-$now"
        scp -P $sinkPort -o "StrictHostKeyChecking no" $username@$ipnodes:/users/$username/receiverlog-$i-$now.tgz .
	ssh -o "StrictHostKeyChecking no" $username@$ipnodes -p "$routerPort" "tar -czvf output-$i-$now.tgz output-$i-$now"
        scp -P $routerPort -o "StrictHostKeyChecking no" $username@$ipnodes:/users/$username/output-$i-$now.tgz .
	echo "Finished copying the output for replication $i"
	lambda=200
	run=1
	ssh -o "StrictHostKeyChecking no" $username@$ipnodes -p "$sinkPort" "pkill -9 ITGRecv"
	sleep 100s
done

echo "Done with all the replications. "
