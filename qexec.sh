#This file needs to be on the router machien on the desktop or as the same location from where you are running experiment runner shel script, 
#if not you will see "No such file found" when you execute experiment-runner.sh 
#Script to clear the queue and reinstall it. 
#This helps to make the previously dropped packets value again to 0 
sudo tc qdisc del dev eth2 root
sudo tc qdisc replace dev eth2 root tbf rate 1mbit limit 25kb burst 32kB peakrate 1.01mbit mtu 1600
