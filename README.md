##BACKGROUND
M/M/1/N is a queueing model in which the arrival and service time distribution are exponential. A grocery counter with a single person providing service to customers can be considered as a simple example of an M/M/1/N queueing model. In the context of networks customers are packet which request service from the server. If the server is busy then packets wait in a queue until preceding packets finish service. If the queue is full then packets are dropped. Note we always operate with utilization greater than 1, that is arrival rate is greater than service rate. We operate in this region since we are interested in studying the blocking probability of M/M/1/N model given by  

|                             |                                                                     |         
|-----------------------------|---------------------------------------------------------------------|
|Pb                           | ![Pb.gif](http://i.imgur.com/AXPHCVn.png)                           |

where N is the number of packets in  the queue,  rho is the utilization of the queue. 
We use blocking probability as given by the second formula. 

##RESULTS
The following results show the blocking probability of M/M/1/N model for different buffer size. We ran the experiment for 15 factor levels with 10 replications for each factor level. In the below figure blocking probability is plotted against buffer size available at queue. Our imposed arrival rate is 220 packets/sec with packet size as 512 bytes. We use equation 1 to evaluate blocking probability analytically using the imposed values. On measuring experimentally we observe that realized mean arrival rate over all iteration is 210.53 packets/sec and mean packet size is 511.833 Bytes. This implies that our intended value of utilization(0.901) and realized utilization(0.862) is different. For lower values of buffer capacity, analytically we get blocking probability to be high and experimentally we get low blocking probability. This occurs since the realized value of arrival rate is lower than what we imposed on the queue. Note that lower realized arrival rate results in less number of packets arriving at queue input than what we intended, which leads to lesser number of packets being dropped from the queue. Thus reducing blocking probability of the queue. From the testbed data we see variations in blocking probability which is expected, however it is within an acceptable range.

![result220.png](https://bitbucket.org/repo/ErqAxR/images/767633173-result220.png)


##RUN MY EXPERIMENT

````
Step 1 : Reserve 3 Geni Nodes
````

Reserve 3 geni nodes with instageni aggregates. All 3 nodes must be on the same site. Lets name these nodes as `sink1` which is the server, `source1` which is the client and `router1` which is the router. Please follow the naming convention since the bashscripts are aligned with this naming. If you want you can give different names, but then make sure to edit those in the bashscript. 


````
Step 2 : Reserve one Public Routable IP Node
````

Reserve one geni node which has public ip. Choose a different site for this than the one you used for your above 3 nodes. To do this drag a VM on jacks, then click on the VM. 
You will get the following on your left handside. `![publicip.png](https://bitbucket.org/repo/ebo9Aj/images/3420014076-publicip.png )`. Check the Public Routable IP.
Name this node as `exec1` 

After you have done this you will see something like this on your jacks view

![topology.png](http://i.imgur.com/PsBley5.png)


````
Step 3 : Set up ssh based authentication for `exec1`
````

Check the status of the nodes on geni portal if ready login to `exec1`. 
Create a ssh keypair using

````
ssh-keygen -t rsa
````

When prompt for password hit enter. Then open a terminal and execute 

````
ssh-copy-id username@ipaddress -p PORT
````
on this node. Here your username means your geniusername. You will run this command thrice , once for each of the 3 geni nodes that you have reserved. In each case the PORT will vary, each time corresponding to the respective node from (sink1,router1 and source1). Here we are trying to create ssh based authentication for `exec1` node to log into the other three nodes. The purpose is to use `exec1` node and keep the experiments running from there. 

Now make sure you are able to ssh into all 3 nodes, `sink1`, `source1`, `router1` node without password. Double check this!!


````
Step 4 : Clone Repository
````

Clone the M/M/1/N repository on `exec1`. cd in to the repository 


````
Step 5 : Install required packages
````

Now after navigating into the repository folder run  
````
sudo apt-get udpate
sudo apt-get -y install vim
sudo apt-get -y install apache2
````
Use vim to open install.sh file (You can use any editor as per your comfort) and there please edit the following
> * `source1` port number
> * `sink1` port number
> *  `username` which implies your geni username
> *  `ipaddress` of the geni nodes 

Make sure you saved the changes that you have done.(Double check). Then run 
````
bash install.sh
````
script on `exec1` node. This will install screen on `exec1` and d-itg traffic generator on `source1` and `sink1`. 

````
Step 6 : Get Ready for Experiment Execution
````
Open `experiment-runner.sh` file using vim. Here please edit the following
> * `sourcePort` : `source1` port number
> * `sinkPort` : `sink1` port number
> * `routerPort` : `router1` port number
> * `username` : your geni username
> * `ipnodes` : ipaddress(common to 3 nodes)

Open `install.sh` file using vim. Here please edit the following
> * `sourcePort` : `source1` port number
> * `sinkPort` : `sink1` port number
> * `username` : your geni username
> * `ipnodes` : ipaddress(common to 3 nodes)

````
Step 7 : Tutorial on using Screen
````
* Now we will do a basic tutorial of screen utility available in linux within 2 minutes
* Screen is a fantastic tool. From [user manual](https://www.gnu.org/software/screen/manual/screen.html) `Screen is a full-screen window manager that multiplexes a physical terminal between several processes, typically interactive shells`
* As we have installed screen in step 5 we are ready to use it. 
* Run `screen` on `exec1` terminal. Then hit `Enter key` when asked for `Press Space or Enter to End`. You will get the user prompt same as you got before. 
* Now if you press `Ctrl +a` and then press `d`, your screen get detached. If you want to re attach the screen run `screen -r`. Note if you run `screen` multiple times on the terminal there will be several sessions open then you have to attach the specific session using `screen -r "session-name"`
* If by mistake you open multiple sessions you can use `killall screen` to kill all the sessions.
* For our experiment we just need one screen session. That's it. 
* As you now have a basic idea of screen let's use it for our experiment. 


````
Step 8 Experiment Execution
````
* In our first lab we used nohup tool.
* Please refer to our lab1 for brief review of it. It is under `tutorial on using linux shell`
* Assuming you are logged in to `exec1` and presently your working directory is the cloned folder please follow the below steps
* Open experiment-runner.sh . Double check your username, ipaddress, port numbers which you have edited in `Step 6`
* Check if this folder contains files called `qmon.sh` and `qexec.sh`
* login to your sink1 node. 
* Use `ps -ef | grep "ITGRecv"` to cross check if there any instance of ITGRecv running. 
* If you found that there is any instance of ITGRecv running please kill it using `kill -9 PID`, where PID is the process ID. 
* If no instance is found logout of the `sink1` node. Now you will be back to your `exec1` node terminal. 
* Here we first check if there is any nohup process being running inthe background. For this execute `ps -ef | grep "nohup"`. If you find any such process kill it using the same command as given in the above step.
* Execute `screen -r`, you  must get output as "There is no screen to be resumed" on your terminal. If there is any session running please kill it using `killall screen`.

* Above steps are sanity checks that we want to perform so as to avoid any errors. 

* In the terminal open a screen session by running `screen`
* Now execute 

````
nohup bash experiment-runner.sh &
````
* The above command start the experiment execution and takes the process in background. 
* Now detach the screen session by pressing keys `Ctrl + a` and then `d`
* Your experiment is running on the screen session, in order to view the experiment progress from anywhere we are gonna follow the below procedure. 
* Execute 

````
chmod a+r nohup.out
````
* Note nohup.out gets created in the same directory once you start the experiment-runner.sh 

* To be able to see your experiment output from anywhere and make sure it's still running, make a symbolic link from your "nohup.out" to a file in the web server directory, e.g. 

````
sudo su
cd /var/www/html/
ln -s /path/to/nohup.out nohup.out
````

* Then, you'll be able to download
http://IP.ADDRESS.OF.GENI.NODE/nohup.out
from anywhere (e.g. your phone) and see how things are going. The IP address is the public IP which you can easily extract using `ifconfig`

````
Step 9 : Check your experiment progress 
````
* The experiment approximately takes 3 hours 
* In the first few minutes you can open view the output of nohup.out in your browser and check if you are detecting any error. 
* If there is any error stop execution 
* Return to Step 8, do all the checks that are necessary and then execute the experiment again. 

````
Step 10 : Data Analysis
````
* After experiment finishes successfully, you will have a three files namely output-`time instance`.tgz, receiverlog-`time instance`.tgz and senderlog-`time-instance`.tgz  and nohup.out in home directory of `exec1` node
* Create a folder called data using `mkdir data`
* move all 4 files to data folder
* run `tar -cvzf data.tgz data`
* Copy all data.tgz to your machine using scp.
* unzip data folder by using command `tar -xvzf data.tgz`
* Similarly unzip the output and receiver folders.
* Note the name of each of the above two folders, we are going to use it shortly
* Download data-processor.sh file from the repository into this current folder
* data-processor.sh takes 3 arguments as listed in below order
> * output folder name `Example output-1-2016-03-16-17-39`
> * receiverlog folder name `Example receiverlog-1-2016-03-16-17-39`
> * csv output filename `Example data.csv`
> * Execute the command `bash data-processor.sh outputfoldername receiverlogfoldername data.csv` 
> * You can process data.csv which is present in  out folder created using R, excel anything that you like.
> * I have done it in R and will give commands for the same


````
Step 11 : Data Visualisation
````
You can  use any data visualization tool of your choice or if you want you can use "Script.m" present in the repository with matlab to see the results as plotted in result section