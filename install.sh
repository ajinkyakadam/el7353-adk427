# Author : Ajinkya Kadam
# Email : ajinkya.kadam@nyu.edu
# Script to install d-itg on source1 and sink1 nodes
# Please edit source port and router port number 
# Please add your geni username and ip address of nodes which can be found on geni portal
 
#!/bin/bash
sudo apt-get -y install screen  
sudo apt-get update
sudo apt-get -y install apache2
sudo apt-get -y install ns2


sourcePort=				## like 	"30268"
sinkPort=				## like 	"30267"
username=				## like 	"zzz000"			your geni username 
ipnodes=				## like 	"pc5.geni.kettering.edu"

echo "Installing d-itg on all nodes. "
ssh -o "StrictHostKeyChecking no" $username@$ipnodes -p "$sourcePort" "sudo apt-get update; sudo apt-get install -y d-itg; sudo apt-get install -y vim"
ssh -o "StrictHostKeyChecking no" $username@$ipnodes -p "$sinkPort" "sudo apt-get update; sudo apt-get install -y d-itg; sudo apt-get install -y vim"

echo "Finished d-itg installation, Please execute the experiment-runner script"
