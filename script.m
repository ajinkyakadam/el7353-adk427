%%x=linspace(10,80,15)
bufferSize= 10:5:80;
x = bufferSize;
rho=0.90;
numer=(1-rho).*((rho).^x);
denom=((1-rho).*(x+1));
Pb=numer./denom;
bufferSize= 10:5:80;
data = csvread('data.csv');

for i=1:15
    data1(:,i) = data(10*(i-1)+1:10*i,4);
end
meanpb = mean(data1);
stdpb = sqrt(var(data1));
confIn =1.96*stdpb/sqrt(10);
errorbar(bufferSize,meanpb,confIn)
hold on
plot(bufferSize,Pb)
